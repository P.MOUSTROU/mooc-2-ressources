# Thématique : Représentation des entiers naturels
**Notions liées :** Données en table de première

**Résumé de l’activité **: Activité débranchée en deux parties. Gestion de données inconvénients et avantages.

**Objectifs :** Découverte des bases de données relationnelles. Introduire le modèle relationnel sans trop de formalisation : relation, attribut, domaine, clef primaire, clef étrangère, schéma relationnel.

**Auteur :** P.MOUSTROU

**Durée de l’activité :** 1h30

**Forme de participation :** élèves en autonomie avec la leçon et exercices. Professeur en accompagnement.

**Matériel nécessaire :** Papier, stylo !

**Préparation :** Aucune

**Autres références :**

**Fiche élève cours :** Représentation des entiers naturels.pdf

**Fiche élève activité :** Exercices dans le cours
